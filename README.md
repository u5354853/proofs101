# Proofs101

I'm running a maths course, based on the open text 
[Book of Proof](https://www.people.vcu.edu/~rhammack/BookOfProof/Main.pdf)
and accompanying [worksheet](https://digitalcommons.plymouth.edu/cgi/viewcontent.cgi?article=1003&context=oer).

I'll assign sections of the text to read and problems to do from the worksheet,
and once a week we will meet as a group over pizza, where I can answer
questions and we can work on interesting problems together.

Old content (including lecture recordings) from when I ran this course last year
avaliable [here](https://cloudstor.aarnet.edu.au/plus/index.php/s/bRnizp3XqotkdJh).

## Workshops

Wednesday, 4:30-5:30pm, N102

Week 2:

Skim chapter 2, make sure you understand implication if you do not already.
Read chapter 4 on direct proof. Do exercises 4.1, 4.2, 4.3, 4.6, 4.7, 4.9 (tricky). 
We will look at similar problems next week. 
Try not to skip too far ahead, as it's hard/unfun to work with someone who's already done all the exercises. 
I estimate this will chew up a few hours. 

If you dont want to commit as much time, skim chapter 4, and have a go at exercises 4.1, 4.2, 4.6.

## Discussion Forum

There's a channel on the [CSSA's discord](https://cssa.club/discord)
that we will be using to chat to each other online.


